/*
 * File:   main.c
 * Author: rbeal
 *
 * Created on December 30, 2017, 9:30 PM
 */

#include <xc.h>
#include "types.h"
#include "defines.h"
#include "ios.h"
#include "soft_timer.h"
#include "timer.h"
#include "usb/usb_device.h"
#include "keyboard.h"

#define SYSTEM_Tasks()

void ioInit(void);
void SYSTEM_Initialize(SYSTEM_STATE state);

int main(void) {
    
    u8 ouaip = 0;
    
    ioInit();
    timer_1_init();
    soft_timer_init();
    
    ios_led_init();
    ios_button_init();
    
    // USB
    SYSTEM_Initialize( SYSTEM_STATE_USB_START );
    USBDeviceInit();
    USBDeviceAttach();
    cirbuf_keyboard_init();
    
    ios_led_setState(LED_1, LED_BLINK);
    
    for (;;) {
        //SYSTEM_Tasks();
        
        
        ios_led_machine();
        ios_button_machine();
        APP_KeyboardTasks();
        
        if (ios_button_getState(BUTTON_2) == BUTTON_PRESSED) {
            if (ouaip == 0) {
                keyboard_putc('r');
                keyboard_putc('o');
                keyboard_putc('u');
                keyboard_putc('t');
                keyboard_putc('e');
                keyboard_putc('\n');
                ouaip = 1;
                ios_led_setState(LED_7, LED_ON);
            }
        }
        else {
            ouaip = 0;
            ios_led_setState(LED_7, LED_OFF);
        }
        
        
        if (ios_button_getState(BUTTON_4) == BUTTON_PRESSED)
            asm("reset"); // For freestyle reset button
    }
    
    return 1;
}


void ioInit(void) {
    
    // Disable alls Analog input
    ANSA = 0;
    ANSB = 0;
    ANSC = 0;
}

void SYSTEM_Initialize(SYSTEM_STATE state) {
    switch(state)
    {
        case SYSTEM_STATE_USB_START:        
            //Make sure that the general purpose output driver multiplexed with
            //the VBUS pin is always consistently configured to be tri-stated in
            //USB applications, so as to avoid any possible contention with the host.
            //(ex: maintain TRISBbits.TRISB6 = 1 at all times).
            TRISBbits.TRISB6 = 1;
            
            
            //BUTTON_Enable(BUTTON_USB_DEVICE_HID_KEYBOARD_KEY);
            break;
            
        case SYSTEM_STATE_USB_SUSPEND:
            //If developing a bus powered USB device that needs to be USB compliant,
            //insert code here to reduce the I/O pin and microcontroller power consumption,
            //so that the total current is <2.5mA from the USB host's VBUS supply.
            //If developing a self powered application (or a bus powered device where
            //official USB compliance isn't critical), nothing strictly needs
            //to be done during USB suspend.

            USBSleepOnSuspend();
            break;

        case SYSTEM_STATE_USB_RESUME:
            //If you didn't change any I/O pin states prior to entry into suspend,
            //then nothing explicitly needs to be done here.  However, by the time
            //the firmware returns from this function, the full application should
            //be restored into effectively exactly the same state as the application
            //was in, prior to entering USB suspend.
            
            //Additionally, before returning from this function, make sure the microcontroller's
            //currently active clock settings are compatible with USB operation, including
            //taking into consideration all possible microcontroller features/effects
            //that can affect the oscillator settings (such as internal/external 
            //switchover (two speed start up), fail-safe clock monitor, PLL lock time,
            //external crystal/resonator startup time (if using a crystal/resonator),
            //etc.

            //Additionally, the official USB specifications dictate that USB devices
            //must become fully operational and ready for new host requests/normal 
            //USB communication after a 10ms "resume recovery interval" has elapsed.
            //In order to meet this timing requirement and avoid possible issues,
            //make sure that all necessary oscillator startup is complete and this
            //function has returned within less than this 10ms interval.

            break;

        default:
            break;
    }
}

void __attribute__((interrupt,auto_psv)) _USB1Interrupt()
{
    USBDeviceTasks();
}

void update_led(void) {
    
    if(USBIsDeviceSuspended() == 1)
    {
        ios_led_setState(LED_2, LED_OFF);
        return;
    }
    
    switch(USBGetDeviceState())
    {         
        case CONFIGURED_STATE:
            /* We are configured.  Blink fast.
             * On for 75ms, off for 75ms, then reset/repeat. */
            
            ios_led_setState(LED_2, LED_BLINK);
            
            break;

        default:
            /* We aren't configured yet, but we aren't suspended so let's blink with
             * a slow pulse. On for 50ms, then off for 950ms, then reset/repeat. */
            
            ios_led_setState(LED_2, LED_ON);
            break;
    }
}