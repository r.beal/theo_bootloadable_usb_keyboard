/* 
 * File:   soft_timer.c
 * Author: rbeal
 *
 * Created on July 30, 2018, 1:42 PM
 */

#include <xc.h>
#include "types.h"
#include "soft_timer.h"

u32 soft_timer_cnt[eNB_TIMERS][2]; // 0 -> cnt, 1 -> period
u8 soft_timer_state[eNB_TIMERS];

void soft_timer_init(void) {
    u8 i;
    
    for (i=0; i<eNB_TIMERS; i++) {
        
        soft_timer_cnt[i][0] = 0;
        soft_timer_cnt[i][1] = 0;
        soft_timer_state[i]  = eTIMER_STOPPED;
    }
}

void soft_timer_machine(void) {
    u8 i;
    
    for (i=0; i<eNB_TIMERS; i++) {
        
        if (soft_timer_state[i] == eTIMER_STOPPED)
            continue;
        
        if (soft_timer_cnt[i][0] > 0 && soft_timer_state[i] == eTIMER_RUNNING) {
            soft_timer_cnt[i][0]--;
        }
        else {
            soft_timer_state[i] = eTIMER_DONE;
        }
    }
}

eSoftTimerStates soft_timer_getState(eSoftTimerList timer) {
    
    return soft_timer_state[timer];
}

void soft_timer_start(eSoftTimerList timer) {
    
    if (soft_timer_cnt[timer][1] == 0)
        return ;
    
    soft_timer_cnt[timer][0] = soft_timer_cnt[timer][1];
    soft_timer_state[timer]  = eTIMER_RUNNING;
}

void soft_timer_stop(eSoftTimerList timer) {
    
    soft_timer_state[timer]  = eTIMER_STOPPED;
    soft_timer_cnt[timer][0] = 0;
}

void soft_timer_set_period(eSoftTimerList timer, u32 period) {
    
    soft_timer_cnt[timer][1] = period;
}