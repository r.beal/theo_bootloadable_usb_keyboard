/* 
 * File:   defines.h
 * Author: rbeal
 *
 * Created on July 30, 2018, 5:24 PM
 */

#ifndef DEFINES_H
#define	DEFINES_H

typedef enum
{
    SYSTEM_STATE_USB_START,
    SYSTEM_STATE_USB_SUSPEND,
    SYSTEM_STATE_USB_RESUME
} SYSTEM_STATE;

void SYSTEM_Initialize(SYSTEM_STATE state);
void update_led(void);

#endif	/* DEFINES_H */

