/* 
 * File:   ios.h
 * Author: rbeal
 *
 * Created on July 30, 2018, 1:41 PM
 */

#ifndef IOS_H
#define	IOS_H

#include "types.h"

#define LED_PERIOD                  (0.5)/SOFT_TIMER_MACHINE_PERIOD // in sec

typedef enum {
    LED_1 = 0,
    LED_2,
    LED_3,
    LED_4,
    LED_5,
    LED_6,
    LED_7,
    LED_8,
    LED_9,
    LED_10,
    NB_LED
} eLed;

typedef enum {
    LED_OFF = 0,
    LED_ON,
    LED_BLINK,
    LED_BLINK_ON,       // DO NOT USE
    LED_BLINK_OFF,      // DO NOT USE
    LED_BLINK_WAIT_ON,  // DO NOT USE
    LED_BLINK_WAIT_OFF  // DO NOT USE
} eLedState;

void ios_led_init(void);
void ios_led_machine(void);
void ios_led_setState(eLed led, eLedState state);
void ios_change_output(u8 lat, u8 val);
void ios_led_toggle(eLed led);

// #############################################################################

typedef enum {
    BUTTON_1 = 0,
    BUTTON_2,
    BUTTON_3,
    BUTTON_4,
    NB_BUTTON
} eButton;

typedef enum {
    BUTTON_PRESSED = 0,
    BUTTON_RELEASED
}eButtonState;

void ios_button_init(void);
void ios_button_machine(void);
eButtonState ios_button_getState(eButton btn);
u8 ios_read_input(u8 lat);

#endif	/* IOS_H */

